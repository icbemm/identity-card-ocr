# Identity Card OCR

Python application that reads data from a romanian identity card and exports it to a .xslx file.

## Installation

These instructions are for **Windows** only.
***
_Please use [**Python 3.9.7**](https://www.python.org/downloads/release/python-397/) for testing this application._
***
1. Using the package manager [pip](https://pip.pypa.io/en/stable/) please install the following:
* torch and torchvision:

If you have a CUDA enabled GPU card use:

```bash
pip3 install torch==1.10.2+cu113 torchvision==0.11.3+cu113 torchaudio===0.10.2+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html
```
or if you intend to run on CPU mode only:

```bash
pip3 install torch torchvision torchaudio
```
_Note: CPU mode is **much slower**._

* And the rest of the dependencies:
```bash
pip install easyocr opencv-python pytesseract unidecode xlsxwriter pandas pyqt5
```
***
2. Next you need to install **Tesseract** using the following [LINK](https://github.com/UB-Mannheim/tesseract/wiki). Make sure to select the right installer for your windows version.

3. Now download the `ron.traineddata` file from [HERE](https://github.com/tesseract-ocr/tessdata/blob/main/ron.traineddata) and place it into the **tessdata** folder found in `C:\Program Files\Tesseract-OCR`. 

4. Please check **pytesseract.pytesseract.tesseract_cmd** PATH and if needed replace `pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'` with your installation path of Tesseract.

**E.g. for Expleo replace `USERNAME` with your username: `C:\\Users\\USERNAME\\AppData\\Local\\Programs\\Tesseract-OCR\\tesseract.exe`**

## Usage

Open a terminal and run:

```bash
python main.py
```

Only accepts _.jpg_, _.jpeg_ and _.png_ files and will create a `.xlsx` at the location of the image that was used.


The GUI makes the use of this application very easy, just browse your image then click `Start` and depending and your GPU/CPU you will have your results in a matter of seconds.

After the results are gathered and shown you can easily **verify** and **modify** the fields if there are any wrong or missing characters. Some fields like `CNP`, `Seria` or `Nr` also have a **visual feedback** when characters or digits are missing.

![8](/uploads/04e1fb49d643886b62bfbe80d892c17d/8.png)

![6](/uploads/36193b255c45214fc0d528f6cbce26fa/6.png)

![7](/uploads/def9c3332f343218a29c91ec4ca3100f/7.png)

If you are happy with your results just click of the `Finish` button and your data will be written in a _.xslx_ file at the location of the image that was used.

![9](/uploads/1e4b0c20431c06766e25e73a8d69fc58/9.png)

**_Note: You will need a good quality photo of your national Identity Card that is neither rotated nor skewed, and has good lighting and no reflections on its surface, otherwise the results may greatly vary._**

## Troubleshooting

- When you run the application for the first time you may encounter an error like the one from below

`'charmap' codec can't encode character '\u0102' in position 9: character maps to <undefined>`

to fix this open a **terminal** and enter the next command:
```bash
export PYTHONIOENCODING=utf8
```
- If you get `Unknown C++ exception from OpenCV code` you need to reinstall openCV:
```bash
pip uninstall opencv-python
pip install opencv-python==4.5.4.60 easyocr
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
