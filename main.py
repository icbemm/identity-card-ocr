import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox
import ci_ocr
from ci_ocr import process_file, process_image

from gui import Ui_MainWindow


class OCR(Ui_MainWindow):
    def __init__(self, appl):
        Ui_MainWindow.__init__(self)
        self.setupUi(appl)
        self.err_msg = QMessageBox()
        self.finishedLabel.setText("")
        self.filepathLabel.setText("")
        self.filename = None
        self.configureEvents()

    def configureEvents(self):
        self.startButton.clicked.connect(self.processImage)
        self.browseButton.clicked.connect(self.browseImage)
        self.finishButton.clicked.connect(self.processFile)

    def browseImage(self):
        try:
            self.filename = QFileDialog.getOpenFileName(None, 'Load Image', '/',
                                                        'Image files (*.jpg *.jpeg *.png)')
            self.filename = self.filename[0]
            if self.filename:
                self.finishedLabel.setStyleSheet("")
                self.extractBox.setEnabled(True)
                self.startButton.setEnabled(True)
                self.finishedLabel.setText("")
                self.filepathLabel.setText("Image at " + self.filename + " loaded successfully.")
            else:
                self.finishedLabel.clear()
                self.finishedLabel.setStyleSheet("")
                self.extractBox.setEnabled(False)
                self.err_msg.setIcon(QMessageBox.Critical)
                self.err_msg.setText("No image was loaded!")
                self.err_msg.setWindowTitle("Error")
                self.err_msg.exec_()
        except Exception as err:
            self.err_msg.setText(str(err))
            self.err_msg.setWindowTitle("Error")
            self.filepathLabel.setText("Last image used: " + self.filename)
            print("browseImage" + str(err))
            self.err_msg.exec_()

    def processImage(self):
        go_on = 0
        wrongs = 0
        try:
            results = process_image(self.filename)
            for i in results:
                if len(i) <= 1:
                    wrongs += 1
            if wrongs >= 8:
                go_on = 1
                self.err_msg.setIcon(QMessageBox.Critical)
                self.err_msg.setText("Please check the image and make sure it's not rotated!")
                self.err_msg.setWindowTitle("OCR ERROR")
                self.finishedLabel.setStyleSheet("background-color: red")
                self.finishedLabel.setText("OCR ERROR!")
                self.startButton.setEnabled(False)
                self.filepathLabel.setText("Last image used: " + self.filename)
                self.err_msg.exec_()

        except Exception as err:
            self.err_msg.setIcon(QMessageBox.Critical)
            self.err_msg.setText(str(err) + "\nCheck the function calls for PyTesseract/EasyOCR.")
            self.err_msg.setWindowTitle("OCR ERROR")
            self.finishedLabel.setStyleSheet("background-color: red")
            self.finishedLabel.setText("OCR ERROR!")
            self.filepathLabel.setText("Last image used: " + self.filename)
            print("processImage: "+ str(err))
            self.err_msg.exec_()

        try:
            if go_on != 1:
                self.gatheredBox.setEnabled(True)
                self.startButton.setEnabled(False)
                self.browseButton.setEnabled(False)
                self.clearText()
                if len(results[0]) > 2:
                    self.lastnameEdit.setText(results[0])
                else:
                    self.lastnameEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.lastnameEdit.setText(results[0])
                if len(results[1]) > 2:
                    self.firstnameEdit.setText(results[1])
                else:
                    self.firstnameEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.firstnameEdit.setText(results[1])
                if len(results[2]) != 13:
                    self.cnpEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.cnpEdit.setText(results[2])
                else:
                    self.cnpEdit.setText(results[2])
                if any(i.isdigit() for i in results[3]) or len(results[3]) != 2:
                    self.seriaEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.seriaEdit.setText(results[3])
                else:
                    self.seriaEdit.setText(results[3])
                if len(results[4]) != 6:
                    self.nrEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.nrEdit.setText(results[4])
                else:
                    self.nrEdit.setText(results[4])
                if results[5] == 'SPCLEP':
                    self.issuedEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.issuedEdit.setText(results[5])
                else:
                    self.issuedEdit.setText(results[5])
                if len(results[6]) <= 1 or len(results[7]) <= 1:
                    self.validityEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.validityEdit.setText(results[6] + " - " + results[7])
                else:
                    self.validityEdit.setText(results[6] + " - " + results[7])
                if len(results[8]) < 3:
                    self.addressEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.addressEdit.setText(results[8])
                else:
                    self.addressEdit.setText(results[8])
                if len(results[9]) < 6:
                    self.nationalityEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.nationalityEdit.setText(results[9])
                else:
                    self.nationalityEdit.setText(results[9])
                if len(results[10]) < 3:
                    self.birthplaceEdit.setStyleSheet("QLineEdit""{""background : red;""}")
                    self.birthplaceEdit.setText(results[10])
                else:
                    self.birthplaceEdit.setText(results[10])
                self.finishedLabel.setStyleSheet("background-color: lightgreen")
                self.finishedLabel.setText("Finished gathering in " + str(ci_ocr.final_time) + "s.")
        except Exception as err:
            self.err_msg.setIcon(QMessageBox.Critical)
            self.err_msg.setText(str(err))
            self.err_msg.setWindowTitle("Error")
            self.filepathLabel.setText("Last image used: " + self.filename)
            print("processImage textEdit: " + str(err))
            self.err_msg.exec_()

    def clearText(self):
        self.finishedLabel.clear()
        self.finishedLabel.setStyleSheet("")
        self.lastnameEdit.clear()
        self.lastnameEdit.setStyleSheet("")
        self.firstnameEdit.clear()
        self.firstnameEdit.setStyleSheet("")
        self.cnpEdit.clear()
        self.cnpEdit.setStyleSheet("")
        self.seriaEdit.clear()
        self.seriaEdit.setStyleSheet("")
        self.nrEdit.clear()
        self.nrEdit.setStyleSheet("")
        self.issuedEdit.clear()
        self.issuedEdit.setStyleSheet("")
        self.validityEdit.clear()
        self.validityEdit.setStyleSheet("")
        self.addressEdit.clear()
        self.addressEdit.setStyleSheet("")
        self.nationalityEdit.clear()
        self.nationalityEdit.setStyleSheet("")
        self.birthplaceEdit.clear()
        self.birthplaceEdit.setStyleSheet("")

    def processFile(self):
        validity = self.validityEdit.text()
        results = \
            [
                self.lastnameEdit.text(),
                self.firstnameEdit.text(),
                self.cnpEdit.text(),
                self.seriaEdit.text(),
                self.nrEdit.text(),
                self.issuedEdit.text(),
                validity[:8],
                validity[11:],
                self.addressEdit.toPlainText(),
                self.nationalityEdit.text(),
                self.birthplaceEdit.text()
            ]
        self.clearText()
        self.gatheredBox.setEnabled(False)
        self.browseButton.setEnabled(True)
        self.filepathLabel.setText("Last image used: " + self.filename)
        process_file(results, self.filename)
        print(results)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    gui = QtWidgets.QMainWindow()
    main = OCR(gui)
    gui.show()
    sys.exit(app.exec_())
