import os
import re
import cv2
import easyocr
import time
import numpy as np
import unidecode as unidecode
import xlsxwriter
import pytesseract

pytesseract.pytesseract.tesseract_cmd = 'C:\\Users\\eiacob\\AppData\\Local\\Programs\\Tesseract-OCR\\tesseract.exe'

RSERIA = r"^[S|s][E|e][R|r][I|i][A|a](\s|.*)[A-Z][A-Z]$"
RSERIA2 = r"^[S|s][E|e][R|r][I|i][A|a](\s|.*)$"
RSERIANR = r"^[A-Z]{2}\d{6}"

RNAME = r"[ABCDEFGHIJKLMNOPQRSTUVWXYZȚȘĂÎÂ]+|-"
RCNP = r"^(\d{13})$"
RCNP2 = r"^\d{7,13}$"
RCNP3 = r"^[C|c][N|n][P|p].\d$"
RNR = r"^(\d{6})$"
# RADDRESS = r"[J|j][U|u][D|d](\.|\s|)([A-Z]{2}).*$"
RADDRESS = r".*[J|j][U|u][D|d](\.|\s|).([A-Z]{2}).*$"
RBIRTHPLACE = r"^[M|m][U|u][N|n](\s|\.).*[J|j][U|u][D|d].*$"
RADDRESS2 = r"^[J|j][U|u][D|d](\.|\s)(\s|\.).*$"
RADDRESS3 = r"^[J|j][U|u][D|d]([A-Z][A-Z]|.)(\s|\.).*$"
RSTR = r"^[S|s][T|t][R|r](\s|\.|).*$"
RAP = r"^ap(.|\s)\d{1,3}$"
RAPT = r"ap(.|\s)\d{1,3}$"
RVALIDITY = r"^(\d{2}).(\d{2}).(\d{2})-.*$"
RVALIDITY2 = r"^.*\d{2}(\.|\s|,)\d{2}(\.|\s|,)\d{2,4}$"
RVALIDITY3 = r".*(\d{2}).(\d{2}).(\d{2})-.*$"
RNATIONALITY = r"^[R|r][O|o][M|m].{3}$"
RISSUED = r"^[S|s][P|p][C|c][L|l][E|e][P|p](\s|.)\b.*$"
RISSUED2 = r"^[S|s].[C|c][L|l][E|e][P|p]"
RMLD = r"^[R|r][E|e][P|p][U|u][B|b].*"

final_time = 0


def biggestRectangle(contours):
    biggest = None
    max_area = 0
    indexReturn = -1
    for index in range(len(contours)):
        i = contours[index]
        area = cv2.contourArea(i)
        if area > 100:
            peri = cv2.arcLength(i, True)
            approx = cv2.approxPolyDP(i, 0.1 * peri, True)
            if area > max_area:
                biggest = approx
                max_area = area
                indexReturn = index
    return indexReturn


def crop(path):
    img = cv2.imread(path)
    height, width, channels = img.shape
    if width in range(1000, 1500) and height in range(800, 1000):
        img = cv2.resize(img, None, fx=0.8, fy=0.8, interpolation=cv2.INTER_CUBIC)
    elif width in range(1500, 5000) and height in range(1000, 5000):
        img = cv2.resize(img, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_CUBIC)
    elif width > 5000 and height > 5000:
        img = cv2.resize(img, None, fx=0.3, fy=0.3, interpolation=cv2.INTER_CUBIC)
    elif width < 1000 and height < 1000:
        img = cv2.resize(img, None, fx=1.3, fy=1.3, interpolation=cv2.INTER_CUBIC)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    invGamma = 1.0 / 0.3
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    gray = cv2.LUT(gray, table)
    ret, thresh1 = cv2.threshold(gray, 60, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]

    indexReturn = biggestRectangle(contours)
    hull = cv2.convexHull(contours[indexReturn])

    mask = np.zeros_like(img)
    cv2.drawContours(mask, contours, indexReturn, 255, -1)
    out = np.zeros_like(img)
    out[mask == 255] = img[mask == 255]

    (y, x, _) = np.where(mask == 255)
    (topy, topx) = (np.min(y), np.min(x))
    (bottomy, bottomx) = (np.max(y), np.max(x))
    out = img[topy: bottomy + 1, topx: bottomx + 1, :]
    out = cv2.bilateralFilter(src=out, d=9, sigmaColor=85, sigmaSpace=85)
    gray = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
    # thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 61, 30)
    thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 55, 38)
    return thresh


def process_image(path):
    start_time = time.time()
    reader = easyocr.Reader(['en', 'ro', 'fr'], gpu=True)
    cropped = crop(path)
    easy_results = reader.readtext(
        path,
        decoder='greedy',
        batch_size=25,
        contrast_ths=20,
        adjust_contrast=70,
        detail=0
    )
    easy_results = easy_results[4:]
    tess_results = pytesseract.image_to_data(
        cropped,
        lang='ron',
        config='--psm 4 --oem 2',
        output_type=pytesseract.Output.DATAFRAME,
    )

    tess_conf_text = tess_results[["conf", "text"]]
    tess_valid = tess_conf_text[tess_conf_text["text"].notnull()]
    try:
        tess_words = tess_valid[tess_valid["text"].str.len() >= 4]
        # tess_conf = tess_words[tess_words["conf"] > 50]
        tess_words = tess_words["text"].to_list()
        tess_predictions = tess_words[4:]
    except AttributeError:
        tess_predictions = []

    final_result = process_words(easy_results, tess_predictions)
    global final_time
    final_time = time.time() - start_time
    final_time = "{:.1f}".format(final_time)
    return final_result


def process_words(words_list, txt):
    seria_found, address_found, birthplace_found, name_found, firstname_found = 0, 0, 0, 0, 0
    nationality_found, validity_found, cnp_found, nr_found = 0, 0, 0, 0
    seria = ""
    cnp = ""
    nr = ""
    last_name = ""
    first_name = ""
    nationality = ""
    birthplace = ""
    address = ""
    issued = ""
    validity = ""
    issue_date = ""
    for i in range(len(words_list)):
        try:
            if re.match(RSERIA, words_list[i]) and seria_found == 0:
                seria = words_list[i]
                seria = seria[6:8]
                seria_found = 1
            elif re.match(RSERIANR, words_list[i]) and seria_found == 0:
                aux = words_list[i]
                seria = aux[:2]
                seria_found = 1
                if nr_found == 0:
                    nr = aux[2:8]
                    nr_found = 1
            if re.match(RSERIA2, words_list[i]) and seria_found == 0:
                if re.match(r"^[A-Z][A-Z]$", words_list[i+1]) and words_list[i+1].lower() != 'nr':
                    seria = words_list[i+1]
                    seria_found = 1
            elif re.match(RNR, words_list[i]) and nr_found == 0:
                nr = words_list[i]
                nr_found = 1
            if len(cnp) < 13:
                if re.match(RCNP3, words_list[i]):
                    aux = words_list[i]
                    cnp = aux[-1] + cnp
            if re.match(RCNP, words_list[i]) and cnp_found == 0:
                cnp = words_list[i]
                cnp_found = 1
            elif re.match(RCNP2, words_list[i]) and cnp_found == 0:
                cnp = cnp + words_list[i]
                cnp_found = 1

        except Exception as err:
            print("Error processing seria/nr/cnp!" + str(err))
        try:
            if ("Nom" in words_list[i] or "Last" in words_list[i] or "last" in words_list[i] or "nom" in words_list[i]) \
                    and name_found == 0:
                if re.match(RNAME, words_list[i + 1]) and not re.match("^.[0-9].*$", words_list[i + 1]):
                    last_name = words_list[i + 1]
                    name_found = 1
                elif re.match(RNAME, words_list[i + 2]) and not re.match("^.[0-9].*$", words_list[i + 2]):
                    last_name = words_list[i + 2]
                    name_found = 1
        except Exception as err:
            print("Error processing first name!" + str(err))
        try:
            if ("Prenu" in words_list[i] or "First" in words_list[i] or "first" in words_list[i] or "ronume" in words_list[i]) \
                    and name_found == 1:
                if "Pronom" in words_list[i + 1] or "Prenom" in words_list[i + 1] or "Prenum" in words_list[i + 1]:
                    if "Pronom" in words_list[i + 2] or "Prenom" in words_list[i + 2]:
                        if re.match(RNAME, words_list[i + 3]):
                            if words_list[i + 3] != last_name and firstname_found == 0:
                                first_name = words_list[i + 3]
                                firstname_found = 1
                    elif re.match(RNAME, words_list[i + 2]):
                        if words_list[i + 2] != last_name and firstname_found == 0:
                            first_name = words_list[i + 2]
                            firstname_found = 1
                elif re.match(RNAME, words_list[i + 1]):
                    if words_list[i + 1] != last_name and firstname_found == 0:
                        first_name = words_list[i + 1]
                        firstname_found = 1
                elif re.match(RNAME, words_list[i + 2]) and firstname_found == 0:
                    first_name = words_list[i + 2]
                    firstname_found = 1
            elif ("Prono" in words_list[i] or "First" in words_list[i] or "first" in words_list[i]
                  or "ronume" in words_list[i] or "Prenu" in words_list[i]) and name_found == 0:
                if "Pronom" in words_list[i + 1] or "Prenom" in words_list[i + 1]:
                    if "Pronom" in words_list[i + 2] or "Prenom" in words_list[i + 2]:
                        if re.match(RNAME, words_list[i + 3]):
                            if firstname_found == 0:
                                first_name = words_list[i + 3]
                                firstname_found = 1
                    elif re.match(RNAME, words_list[i + 2]):
                        if firstname_found == 0:
                            first_name = words_list[i + 2]
                            firstname_found = 1
                elif re.match(RNAME, words_list[i + 1]):
                    if firstname_found == 0:
                        first_name = words_list[i + 1]
                        firstname_found = 1

                elif re.match(RNAME, words_list[i + 2]) and firstname_found == 0:
                    first_name = words_list[i + 2]
                    firstname_found = 1
        except Exception as err:
            print("Error processing first name!" + str(err))
        try:
            if re.match(RNATIONALITY, words_list[i]) and nationality_found == 0 and words_list[i] == 'Română':
                nationality = words_list[i]
                nationality_found = 1
            else:
                nationality = 'Română'
                nationality_found = 1
        except Exception as err:
            print("Error processing nationality!" + str(err))
        try:
            if ("Loc na" in words_list[i] or "naissance" in words_list[i] or "birth" in words_list[i]) \
                    and birthplace_found == 0:
                if re.match(RADDRESS, words_list[i + 1]) or re.match(RBIRTHPLACE, words_list[i + 1]) or re.match(
                        RADDRESS3, words_list[i + 1]):
                    birthplace = words_list[i + 1]
                    birthplace_found = 1
                elif re.match(RMLD, words_list[i + 1]):
                    birthplace = words_list[i + 1]
                    birthplace_found = 1
            elif birthplace_found == 0 and 'Jud' in words_list[i]:
                birthplace = words_list[i]
                birthplace_found = 1
        except Exception as err:
            print("Error processing birthplace!" + str(err))
        try:
            if ("Domicil" in words_list[i] or "Address" in words_list[i] or "address" in words_list[i] or "Addr" in
                    words_list[i]) and birthplace_found == 1:
                if (re.match(RADDRESS, words_list[i + 1]) or re.match(RADDRESS2, words_list[i + 1]) or re.match(RADDRESS3, words_list[i + 1])) \
                        and address_found == 0:
                    if 'sc.' in words_list[i + 2] or 'et.' in words_list[i + 2] or 'bl.' in words_list[i + 2]:
                        address = words_list[i + 1] + " " + words_list[i + 2]
                        address_found = 1
                        if 'ap.' in words_list[i + 3]:
                            address = address + " " + words_list[i + 3]
                            address_found = 2
                    elif 'ap.' in words_list[i + 2]:
                        if 'sc.' in words_list[i + 3] or 'et.' in words_list[i + 3] or 'bl.' in words_list[i + 3]:
                            address = words_list[i + 1] + " " + words_list[i + 3] + " " + words_list[i + 2]
                            address_found = 2
                    elif re.match(r"^[N|n][R|r](\.|).*$", words_list[i + 2]):
                        if 'nr' not in address:
                            address = words_list[i + 1] + " " + words_list[i + 2]
                            address_found = 2
                    else:
                        address = words_list[i + 1]
                        address_found = 1
                elif re.match(RBIRTHPLACE, words_list[i + 1]) and address_found == 0:
                    address = words_list[i + 1]
                    address_found = 1
                    if 'bl.' in words_list[i + 2] or 'sc.' in words_list[i + 2] or 'ap.' in words_list[i + 2] \
                            and address_found == 1:
                        address = address + " " + words_list[i + 2]
                        address_found = 2
                if re.match(RSTR, words_list[i + 2]) and address_found == 1:
                    if words_list[i + 2] not in address:
                        address = address + " " + words_list[i + 2]
                        address_found = 2
                elif re.match(RSTR, words_list[i + 3]) and address_found == 1:
                    if words_list[i + 3] not in address:
                        address = address + " " + words_list[i + 3]
                        address_found = 2
                elif 'bl.' in words_list[i + 2] or 'sc.' in words_list[i + 2] or 'ap.' in words_list[i + 2] \
                                and address_found == 1:
                    if not words_list[i + 2] in address:
                        address = address + " " + words_list[i + 2]
                        address_found = 2
            elif ("Domicil" in words_list[i] or "Address" in words_list[i] or "address" in words_list[i]) and birthplace_found == 0:
                if (re.match(RADDRESS, words_list[i + 1]) or re.match(RADDRESS2, words_list[i + 1]) or re.match(RADDRESS3, words_list[i + 1]))\
                        and address_found == 0:
                    if 'sc.' in words_list[i + 2] or 'et.' in words_list[i + 2] or 'bl.' in words_list[i + 2]:
                        address = words_list[i + 1] + " " + words_list[i + 2]
                        if 'ap.' in words_list[i + 3]:
                            address = address + " " + words_list[i + 3]
                    elif 'ap.' in words_list[i + 2]:
                        if 'sc.' in words_list[i + 3] or 'et.' in words_list[i + 3] or 'bl.' in words_list[i + 3]:
                            address = words_list[i + 1] + " " + words_list[i + 3] + " " + words_list[i + 2]
            elif re.match(RAP, words_list[i]) or re.match(RAPT, words_list[i]):
                if words_list[i] not in address:
                    address = address + " " + words_list[i]
        except Exception as err:
            print("Error processing address!" + str(err))
        try:
            if re.match(RISSUED, words_list[i]):
                issued = words_list[i]
        except Exception as err:
            print("Error processing issued by!" + str(err))
        try:
            if re.match(RVALIDITY, words_list[i]):
                validity = words_list[i]
                issue_date = validity[:8]
                validity = validity[9:]
                validity_found = 1
            elif re.match(RVALIDITY2, words_list[i]) and validity_found == 0:
                validity = words_list[i]
                validity = validity[-10:]
                validity_found = 2
        except Exception as err:
            print("Error processing validity!" + str(err))
        if address_found == 2 and re.match(RAP, words_list[i]) and 'ap.' not in address:
            address = address + " " + words_list[i]
        elif birthplace_found == 0 and re.match(RADDRESS, words_list[i]):
            birthplace = words_list[i]
            birthplace_found = 1
        elif address_found == 0 and birthplace_found == 1 and re.match(RADDRESS, words_list[i]):
            if words_list[i] not in address:
                address = words_list[i]
    if name_found == 0:
        for i in range(len(words_list)):
            if re.match(RNAME, words_list[i]) and 'IDENTIT' not in words_list[i] and 'CARTE' not in words_list[i] \
                    and 'CARD' not in words_list[i]:
                if "<" in words_list[i]:
                    last_name = ""
                else:
                    last_name = words_list[i]
                    name_found = 1
    if firstname_found == 0:
        for i in range(len(words_list)):
            if re.match(RNAME, words_list[i]) and 'IDENTIT' not in words_list[i] and 'CARTE' not in words_list[i] \
                    and 'CARD' not in words_list[i]:
                if "<" in words_list[i]:
                    first_name = ""
                elif name_found == 1:
                    first_name = words_list[i]
    if len(seria) != 2 or not re.match(r"[A-Z][A-Z]", seria):
        seria = words_list[-1]
        if not re.match(r"\d", seria[:2]) and (not seria[:2].islower() and not seria[:2].isupper()):
            seria = seria[:2]
        else:
            seria = ""
    if len(issued) < 2:
        issued = "SPCLEP"
    if len(nr) != 6:
        aux = words_list[-1]
        nr = aux[2:8]

    try:
        if txt:
            for i in range(len(txt)):
                if "Nom" in txt[i] and name_found == 1:
                    if "name" in txt[i + 1] and re.match(RNAME, txt[i + 2]) and not re.match("^.[0-9].*$", txt[i + 2]):
                        if txt[i + 2] > last_name and unidecode.unidecode(txt[i + 2]) == last_name:
                            last_name = txt[i + 2]
                            name_found = 2
                    elif "name" in txt[i + 1] and re.match(RNAME, txt[i + 1]) and not re.match("^.[0-9].*$",
                                                                                               txt[i + 1]):
                        if txt[i + 1] > last_name and unidecode.unidecode(txt[i + 1]) == last_name:
                            last_name = txt[i + 1]
                            name_found = 2
                    elif "name" in txt[i + 1] and re.match(RNAME, txt[i + 3]) and not re.match("^.[0-9].*$",
                                                                                               txt[i + 3]):
                        if txt[i + 3] > last_name and unidecode.unidecode(txt[i + 3]) == last_name:
                            last_name = txt[i + 3]
                            name_found = 2
                elif ("renume" in txt[i] or "Prenom" in txt[i]) and name_found == 2:
                    if "name" in txt[i + 1] and re.match(RNAME, txt[i + 2]):
                        if txt[i + 2] > first_name and unidecode.unidecode(txt[i + 2]) == first_name:
                            first_name = txt[i + 2]
                elif 'name' in txt[i] and (name_found == 1 or name_found == 2):
                    if re.match(RNAME, txt[i + 1]) and firstname_found == 0:
                        first_name = txt[i + 1]
                    elif re.match(RNAME, txt[i + 1]) and firstname_found == 1:
                        if txt[i + 1] > first_name and unidecode.unidecode(txt[i + 1]) == first_name:
                            first_name = txt[i + 1]
                            firstname_found = 2
                elif ("name" in txt[i] or "ame" in txt[i]) and (name_found == 1 or name_found == 2):
                    if re.match(RNAME, txt[i + 1]):
                        if txt[i + 1] > last_name and unidecode.unidecode(txt[i + 1]) == last_name:
                            last_name = txt[i + 1]
                            name_found = 2
                        elif txt[i + 2] > last_name and unidecode.unidecode(txt[i + 2]) == last_name:
                            last_name = txt[i + 2]
                            name_found = 2
                        elif txt[i + 1] > first_name and unidecode.unidecode(txt[i + 1]) == first_name:
                            first_name = txt[i + 1]
                elif unidecode.unidecode(last_name) == unidecode.unidecode(txt[i]):
                    if txt[i] > last_name:
                        last_name = txt[i]
                elif unidecode.unidecode(first_name) == unidecode.unidecode(txt[i]):
                    if txt[i] > first_name:
                        first_name = txt[i]
                if len(cnp) < 13:
                    if re.match(RCNP, txt[i]):
                        cnp = txt[i]
                if len(seria) != 2 or any(i.isdigit() for i in seria):
                    if re.match(r"^[A-Z]{2}\d{6}.*", txt[-1]):
                        seria = txt[-1]
                        seria = seria[:2]
                    elif re.match(r"^[A-Z]{2}\d{6}.*", txt[-2]):
                        seria = txt[-2]
                        seria = seria[:2]
                if len(nr) != 6:
                    if re.match(RNR, txt[i]):
                        nr = txt[i]
                    else:
                        aux = txt[-1]
                        aux = aux[2:8]
                        nr = aux
                if re.match(RVALIDITY, txt[i]) and validity_found == 2:
                    validity = txt[i]
                    issue_date = validity[:8]
                    validity = validity[9:]
                elif re.match(RVALIDITY3, txt[i]) and validity_found == 2:
                    validity = txt[i]
                    validity = validity[-19:]
                    issue_date = validity[:8]
                    validity = validity[9:]
                if issued == 'SPCLEP' and re.match(RISSUED2, txt[i]):
                    if txt[i + 1] != issue_date + validity:
                        issued = issued + " " + txt[i + 1]
                elif re.match(RISSUED2, txt[i]):
                    if txt[i + 1] != issue_date + validity:
                        aux = 'SPCLEP' + " " + txt[i + 1]
                        if unidecode.unidecode(aux) == unidecode.unidecode(issued):
                            if aux > issued:
                                issued = aux
    except Exception as err:
        print("Error processing data from pytesseract!" + str(err))
    results = [last_name, first_name, cnp, seria, nr, issued, issue_date, validity, address, nationality, birthplace]
    return results


def process_file(words, path):
    new_path = os.path.dirname(path)
    with xlsxwriter.Workbook(new_path + "/" + f'{words[0] + "_" + words[1]}.xlsx') as workbook:
        worksheet = workbook.add_worksheet()
        bold = workbook.add_format({'bold': 1})
        worksheet.set_column(0, 2, 15)
        worksheet.set_column(4, 4, 10)
        worksheet.set_column(5, 10, 20)
        worksheet.write('A1', 'Nume', bold)
        worksheet.write('B1', 'Prenume', bold)
        worksheet.write('C1', 'CNP', bold)
        worksheet.write('D1', 'Serie CI', bold)
        worksheet.write('E1', 'Numar CI', bold)
        worksheet.write('F1', 'Org eliberare CI', bold)
        worksheet.write('G1', 'Data eliberare CI', bold)
        worksheet.write('H1', 'Valabilitate CI', bold)
        worksheet.write('I1', 'Adresa', bold)
        worksheet.write('J1', 'Cetatenie', bold)
        worksheet.write('K1', 'Locul nasterii', bold)
        for col in range(len(words)):
            worksheet.write_string(1, col, words[col])
